#ifndef FXP_META_H
#define FXP_META_H
//#include <ap_fixed.h>
#include <fixedpoint/fixed_point.hpp>
//below makes new lib compatiple to vivado headers
//Vivado :ap_fixed<WL,IW> ;// FixedPoint<IW,FW>
#define ap_fixed FixedPoint
#define ap_temp_fixed_1 ap_fixed<7,10>
#define ap_temp_fixed_2 ap_fixed<2,6>
#define ap_temp_fixed_3 ap_fixed<2,6>
#define ap_temp_fixed_4 ap_fixed<7,6>
#define ap_temp_fixed_5 ap_fixed<7,0>
#define ap_temp_fixed_6 ap_fixed<2,0>
#define ap_temp_fixed_7 ap_fixed<2,0>
#define ap_temp_fixed_8 ap_fixed<2,0>
#define ap_temp_fixed_9 ap_fixed<2,2>
#define ap_temp_fixed_10 ap_fixed<7,7>
#define ap_temp_fixed_11 ap_fixed<2,1>
#define ap_temp_fixed_12 ap_fixed<7,6>
#define ap_temp_fixed_13 ap_fixed<2,2>
#define ap_temp_fixed_14 ap_fixed<7,8>
#define ap_temp_fixed_15 ap_fixed<7,10>
#define ap_temp_fixed_16 ap_fixed<2,2>
#endif