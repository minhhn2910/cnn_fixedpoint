#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Processing header file and compile fixed point program.
input:
    + output file name
    + iw_config.txt contains integer word length of the variables
    + fr_config.txt contains fraction bitwidth of the variables
    + fxp_meta.h header file contains meta-data for fixedpoint variables declaration
output:
    + processed fxp_meta.h
    + compiled binary file
Written by Minh Ho (minhhn2910(at)gmail.com)
(c) Copyright, All Rights Reserved. NO WARRANTY.
"""
import os
import sys
import subprocess
import shlex
vivado_mode = False
def read_config_file(config_file_name):
    return_list = []
    with open(config_file_name, 'r') as conf_file:
        for line in conf_file:
            line.replace(" ", "")
            array = line.split(',')
            for element in array:
                try:
            #        print element
                    if(len(element)>0 and element!='\n'):
                        return_list.append(int(element))
                except:
                    pass
                    #print "Failed to parse config file"
	return 	return_list
def write_header_file(iw_list,fr_list):
	header_lines = []
	with open('temp_fxp_meta.h', 'r') as header_file:
		for line in header_file:
			header_lines.append(line.replace('\n',''))
    #skip first line #include <ap_fixed.h>
	count = 0;
	for i in range(len(header_lines)):
		if (count>(len(iw_list)-1)):
			break
		if("ap_temp_fixed" in header_lines[i]):
			if(vivado_mode):
				header_lines[i] = header_lines[i]+ ' ap_fixed<%s,%s>\n'%(fr_list[count]+iw_list[count],iw_list[count])
			else:
				header_lines[i] = header_lines[i]+ ' ap_fixed<%s,%s>\n'%(iw_list[count],fr_list[count])
			count = count + 1

		else:
			header_lines[i] = header_lines[i] + '\n' #put back \n
	header_file = open('fxp_meta.h', 'w')
	for line in header_lines:
		header_file.write(line)


def main():
    iw_list = read_config_file('iw_config.txt')
    fr_list = read_config_file('fr_config.txt')
#    try:
#        os.remove('fxp_meta.h')
#        os.remove('test')
#    except:
#        pass
    write_header_file(iw_list,fr_list)
    #command = 'g++ -o %s %s -I/home/minh/include -I./'%(output_file,output_file + '.cpp')
    command = './compile.sh'
    args = shlex.split(command)
    output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]

if __name__ == '__main__':
	arguments = sys.argv[1:]
    #main()
	#if len(arguments)!=1:
	#	print "usage ./process_header.py output_binary_file"
	main()
