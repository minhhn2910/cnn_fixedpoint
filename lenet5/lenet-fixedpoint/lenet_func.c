#include "lenet.h"
#include <memory.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "fxp_meta.h"
// Replace define by functions , better for code analyzing
#define GETLENGTH(array) (sizeof(array)/sizeof(*(array)))

#define GETCOUNT(array)  (sizeof(array)/sizeof(double))

#define FOREACH(i,count) for (int i = 0; i < count; ++i)

#define CONVOLUTE_VALID(input,output,weight)											\
{																						\
	FOREACH(o0,GETLENGTH(output))														\
		FOREACH(o1,GETLENGTH(*(output)))												\
			FOREACH(w0,GETLENGTH(weight))												\
				FOREACH(w1,GETLENGTH(*(weight)))										\
					(output)[o0][o1] += (input)[o0 + w0][o1 + w1] * (weight)[w0][w1];	\
}

#define CONVOLUTE_FULL(input,output,weight)												\
{																						\
	FOREACH(i0,GETLENGTH(input))														\
		FOREACH(i1,GETLENGTH(*(input)))													\
			FOREACH(w0,GETLENGTH(weight))												\
				FOREACH(w1,GETLENGTH(*(weight)))										\
					(output)[i0 + w0][i1 + w1] += (input)[i0][i1] * (weight)[w0][w1];	\
}

#define CONVOLUTION_FORWARD(input,output,weight,bias,action)					\
{																				\
	for (int x = 0; x < GETLENGTH(weight); ++x)									\
		for (int y = 0; y < GETLENGTH(*weight); ++y)							\
			CONVOLUTE_VALID(input[x], output[y], weight[x][y]);					\
	FOREACH(j, GETLENGTH(output))												\
		FOREACH(i, GETCOUNT(output[j]))											\
		((double *)output[j])[i] = action(((double *)output[j])[i] + bias[j]);	\
}

#define CONVOLUTION_BACKWARD(input,inerror,outerror,weight,wd,bd,actiongrad)\
{																			\
	for (int x = 0; x < GETLENGTH(weight); ++x)								\
		for (int y = 0; y < GETLENGTH(*weight); ++y)						\
			CONVOLUTE_FULL(outerror[y], inerror[x], weight[x][y]);			\
	FOREACH(i, GETCOUNT(inerror))											\
		((double *)inerror)[i] *= actiongrad(((double *)input)[i]);			\
	FOREACH(j, GETLENGTH(outerror))											\
		FOREACH(i, GETCOUNT(outerror[j]))									\
		bd[j] += ((double *)outerror[j])[i];								\
	for (int x = 0; x < GETLENGTH(weight); ++x)								\
		for (int y = 0; y < GETLENGTH(*weight); ++y)						\
			CONVOLUTE_VALID(input[x], wd[x][y], outerror[y]);				\
}


#define SUBSAMP_MAX_FORWARD(input,output)														\
{																								\
	const int len0 = GETLENGTH(*(input)) / GETLENGTH(*(output));								\
	const int len1 = GETLENGTH(**(input)) / GETLENGTH(**(output));								\
	FOREACH(i, GETLENGTH(output))																\
	FOREACH(o0, GETLENGTH(*(output)))															\
	FOREACH(o1, GETLENGTH(**(output)))															\
	{																							\
		int x0 = 0, x1 = 0, ismax;																\
		FOREACH(l0, len0)																		\
			FOREACH(l1, len1)																	\
		{																						\
			ismax = input[i][o0*len0 + l0][o1*len1 + l1] > input[i][o0*len0 + x0][o1*len1 + x1];\
			x0 += ismax * (l0 - x0);															\
			x1 += ismax * (l1 - x1);															\
		}																						\
		output[i][o0][o1] = input[i][o0*len0 + x0][o1*len1 + x1];								\
	}																							\
}

#define SUBSAMP_MAX_BACKWARD(input,inerror,outerror)											\
{																								\
	const int len0 = GETLENGTH(*(inerror)) / GETLENGTH(*(outerror));							\
	const int len1 = GETLENGTH(**(inerror)) / GETLENGTH(**(outerror));							\
	FOREACH(i, GETLENGTH(outerror))																\
	FOREACH(o0, GETLENGTH(*(outerror)))															\
	FOREACH(o1, GETLENGTH(**(outerror)))														\
	{																							\
		int x0 = 0, x1 = 0, ismax;																\
		FOREACH(l0, len0)																		\
			FOREACH(l1, len1)																	\
		{																						\
			ismax = input[i][o0*len0 + l0][o1*len1 + l1] > input[i][o0*len0 + x0][o1*len1 + x1];\
			x0 += ismax * (l0 - x0);															\
			x1 += ismax * (l1 - x1);															\
		}																						\
		inerror[i][o0*len0 + x0][o1*len1 + x1] = outerror[i][o0][o1];							\
	}																							\
}

#define DOT_PRODUCT_FORWARD(input,output,weight,bias,action)				\
{																			\
	for (int x = 0; x < GETLENGTH(weight); ++x)								\
		for (int y = 0; y < GETLENGTH(*weight); ++y)						\
			((double *)output)[y] += ((double *)input)[x] * weight[x][y];	\
	FOREACH(j, GETLENGTH(bias))												\
		((double *)output)[j] = action(((double *)output)[j] + bias[j]);	\
}

#define DOT_PRODUCT_BACKWARD(input,inerror,outerror,weight,wd,bd,actiongrad)	\
{																				\
	for (int x = 0; x < GETLENGTH(weight); ++x)									\
		for (int y = 0; y < GETLENGTH(*weight); ++y)							\
			((double *)inerror)[x] += ((double *)outerror)[y] * weight[x][y];	\
	FOREACH(i, GETCOUNT(inerror))												\
		((double *)inerror)[i] *= actiongrad(((double *)input)[i]);				\
	FOREACH(j, GETLENGTH(outerror))												\
		bd[j] += ((double *)outerror)[j];										\
	for (int x = 0; x < GETLENGTH(weight); ++x)									\
		for (int y = 0; y < GETLENGTH(*weight); ++y)							\
			wd[x][y] += ((double *)input)[x] * ((double *)outerror)[y];			\
}

double relu(double x)
{
	return x*(x > 0);
}

ap_temp_fixed_16 relu_fixed(ap_temp_fixed_16 x)
{
	//return x*(x > 0);
	if(x> ap_temp_fixed_16(0)) return x;
	else return ap_temp_fixed_16(0);
}

double relugrad(double y)
{
	return y > 0;
}

//define for code analyzing

void DOT_PRODUCT_FORWARD_test(double ***input,double *output,double **weight,double *bias)				
{																		
	for (int x = 0; x < GETLENGTH(weight); ++x)								
		for (int y = 0; y < GETLENGTH(*weight); ++y)						
			((double *)output)[y] += ((double *)input)[x] * weight[x][y];	
	FOREACH(j, GETLENGTH(bias))												
		((double *)output)[j] = relu(((double *)output)[j] + bias[j]);	
}


void SUBSAMP_MAX_FORWARD_test(double ***input,double ***output)		//3d 										
{																								
	const int len0 = GETLENGTH(*(input)) / GETLENGTH(*(output));								
	const int len1 = GETLENGTH(**(input)) / GETLENGTH(**(output));								
	FOREACH(i, GETLENGTH(output))																
	FOREACH(o0, GETLENGTH(*(output)))															
	FOREACH(o1, GETLENGTH(**(output)))															
	{																							
		int x0 = 0, x1 = 0, ismax;																
		FOREACH(l0, len0)																		
			FOREACH(l1, len1)																	
		{																						
			ismax = input[i][o0*len0 + l0][o1*len1 + l1] > input[i][o0*len0 + x0][o1*len1 + x1];
			x0 += ismax * (l0 - x0);															
			x1 += ismax * (l1 - x1);															
		}																						
		output[i][o0][o1] = input[i][o0*len0 + x0][o1*len1 + x1];								
	}																							
}

void CONVOLUTE_VALID_test(double **input,double **output,double **weight)		//2d									
{																						
	FOREACH(o0,GETLENGTH(output))														
		FOREACH(o1,GETLENGTH(*(output)))												
			FOREACH(w0,GETLENGTH(weight))												
				FOREACH(w1,GETLENGTH(*(weight)))										
					(output)[o0][o1] += (input)[o0 + w0][o1 + w1] * (weight)[w0][w1];	
}

//~ void CONVOLUTION_FORWARD_test(double input[][][],double output[][][],double weight[][][][],double bias[])	  				
//~ {																	
	//~ for (int x = 0; x < GETLENGTH(weight); ++x)									
		//~ for (int y = 0; y < GETLENGTH(*weight); ++y)							
			//~ CONVOLUTE_VALID_test(input[x], output[y], weight[x][y]);					
	//~ FOREACH(j, GETLENGTH(output))												
		//~ FOREACH(i, GETCOUNT(output[j]))	;										
		//~ ((double *)output[j])[i] = relu(((double *)output[j])[i] + bias[j]);	//replace action with relu.
//~ }
//



static void forward(LeNet5 *lenet, Feature *features, double(*action)(double))
{
	CONVOLUTION_FORWARD(features->input, features->layer1, lenet->weight0_1, lenet->bias0_1, action);
	SUBSAMP_MAX_FORWARD(features->layer1, features->layer2);
	CONVOLUTION_FORWARD(features->layer2, features->layer3, lenet->weight2_3, lenet->bias2_3, action);
	SUBSAMP_MAX_FORWARD(features->layer3, features->layer4);
	CONVOLUTION_FORWARD(features->layer4, features->layer5, lenet->weight4_5, lenet->bias4_5, action);
	DOT_PRODUCT_FORWARD(features->layer5, features->output, lenet->weight5_6, lenet->bias5_6, action);
}

//~ static void forward_testing(LeNet5 *lenet, Feature *features)
//~ {
		//~ printf(" %x \n", features->input[0][0][0]);			
	//~ CONVOLUTION_FORWARD_test(features->input, features->layer1, lenet->weight0_1, lenet->bias0_1);
	//~ SUBSAMP_MAX_FORWARD_test(features->layer1, features->layer2);
	//~ CONVOLUTION_FORWARD_test(features->layer2, features->layer3, lenet->weight2_3, lenet->bias2_3);
	//~ SUBSAMP_MAX_FORWARD_test(features->layer3, features->layer4);
	//~ CONVOLUTION_FORWARD_test(features->layer4, features->layer5, lenet->weight4_5, lenet->bias4_5);
	//~ DOT_PRODUCT_FORWARD_test(features->layer5, features->output, lenet->weight5_6, lenet->bias5_6);
//~ }
 void forward_testing(LeNet5 *lenet, LeNet5_fixed* lenet_fixed, Feature *features, Feature_fixed* features_fixed)
{
	
	//conv1 
 {
//	ap_temp_fixed_1 layer1_temp;
//	ap_temp_fixed_1 weigh0_1_temp;
	//input[INPUT][LENGTH_FEATURE0][LENGTH_FEATURE0]; //[1][32][32]  1024
//	ap_temp_fixed_1 input_temp [INPUT][LENGTH_FEATURE0][LENGTH_FEATURE0]; 
/*	for(int i=0;i<INPUT;i++)
		for(int j=0;j<LENGTH_FEATURE0;j++)
			for(int k=0;k<LENGTH_FEATURE0;k++){
				input_temp[i][j][k] = features->input[i][j][k];
				}
	*/		
	
	for (int x = 0; x < (sizeof(lenet->weight0_1)/sizeof(*(lenet->weight0_1))); ++x)
	 for (int y = 0; y < (sizeof(*lenet->weight0_1)/sizeof(*(*lenet->weight0_1))); ++y) 
		for (int o0 = 0; o0 < (sizeof(features->layer1[y])/sizeof(*(features->layer1[y]))); ++o0) 
		 for (int o1 = 0; o1 < (sizeof(*(features->layer1[y]))/sizeof(*(*(features->layer1[y])))); ++o1)
		{
			//int x= 0;
			//~ for (int x = 0; x < (sizeof(lenet->weight0_1)/sizeof(*(lenet->weight0_1))); ++x)
			
		 //~ {
			 //~ int y = 1; int o0 = 5; int o1 = 5;
			 
			// layer1_temp = features->layer1[y][o0][o1];
			for (int w0 = 0; w0 < (sizeof(lenet->weight0_1[x][y])/sizeof(*(lenet->weight0_1[x][y]))); ++w0) 
				for (int w1 = 0; w1 < (sizeof(*(lenet->weight0_1[x][y]))/sizeof(*(*(lenet->weight0_1[x][y])))); ++w1){
			//			printf("before %.2f %.2f %.2f \n",features_fixed->layer1[y][o0][o1] .getValueF(),   features_fixed->input[x][o0 + w0][o1 + w1] .getValueF(), lenet_fixed->weight0_1[x][y][w0][w1] .getValueF());
					features_fixed->layer1[y][o0][o1] += features_fixed->input[x][o0 + w0][o1 + w1] * lenet_fixed->weight0_1[x][y][w0][w1];
					//~ ap_temp_fixed_1 temp_val = (features_fixed->input[x][o0 + w0][o1 + w1] * lenet_fixed->weight0_1[x][y][w0][w1]);
					//~ features_fixed->layer1[y][o0][o1]  += temp_val;
					//~ float input = features_fixed->input[x][o0 + w0][o1 + w1].getValueF();
					//~ float weight = lenet_fixed->weight0_1[x][y][w0][w1].getValueF();
					//~ features_fixed->layer1[y][o0][o1] += input*weight;
					//	layer1_temp += input_temp[x][o0 + w0][o1 + w1] * lenet_fixed->weight0_1[x][y][w0][w1];
		//					printf("after %.2f %.2f %.2f \n",features_fixed->layer1[y][o0][o1] .getValueF(),   features_fixed->input[x][o0 + w0][o1 + w1] .getValueF(), lenet_fixed->weight0_1[x][y][w0][w1] .getValueF());
					} 
			//features->layer1[y][o0][o1] = layer1_temp;
		}
					
	for (int j = 0; j < (sizeof(features->layer1)/sizeof(*(features->layer1))); ++j)
		for (int i = 0; i < (sizeof(features->layer1[j])/sizeof(double)); ++i){
			//~ ((double *)features_fixed->layer1[j])[i] = relu(((double *)features_fixed->layer1[j])[i] + lenet_fixed->bias0_1[j]);
			((ap_temp_fixed_10 *)features_fixed->layer1[j])[i] = relu_fixed(((ap_temp_fixed_10 *)features_fixed->layer1[j])[i] + lenet_fixed->bias0_1[j]);
			//~ ap_temp_fixed_6 relu_arg = ap_temp_fixed_6((((ap_temp_fixed_4 *)features_fixed->layer1[j])[i] + lenet_fixed->bias0_1[j]).getValueF());
			 //~ ap_temp_fixed_6 relu_result =  relu_fixed(relu_arg);
				//~ ((ap_temp_fixed_4 *)features_fixed->layer1[j])[i] = ap_temp_fixed_4(relu_result.getValueF());
		}
	
		
 };
 
 
//sub sampling
 { 	const int len0 = (sizeof(*(features->layer1))/sizeof(*(*(features->layer1)))) / (sizeof(*(features->layer2))/sizeof(*(*(features->layer2))));
	const int len1 = (sizeof(**(features->layer1))/sizeof(*(**(features->layer1)))) / (sizeof(**(features->layer2))/sizeof(*(**(features->layer2))));
	for (int i = 0; i < (sizeof(features->layer2)/sizeof(*(features->layer2))); ++i)
		for (int o0 = 0; o0 < (sizeof(*(features->layer2))/sizeof(*(*(features->layer2)))); ++o0) 
			for (int o1 = 0; o1 < (sizeof(**(features->layer2))/sizeof(*(**(features->layer2)))); ++o1) {
				int x0 = 0, x1 = 0, ismax;
				for (int l0 = 0; l0 < len0; ++l0) for (int l1 = 0; l1 < len1; ++l1) { 
					ismax = features_fixed->layer1[i][o0*len0 + l0][o1*len1 + l1] > features_fixed->layer1[i][o0*len0 + x0][o1*len1 + x1];
					x0 += ismax * (l0 - x0);
					x1 += ismax * (l1 - x1); 
				} 
					features_fixed->layer2[i][o0][o1] = features_fixed->layer1[i][o0*len0 + x0][o1*len1 + x1];
			} 
 };
 //conv2 
 { for (int x = 0; x < (sizeof(lenet->weight2_3)/sizeof(*(lenet->weight2_3))); ++x)
	  for (int y = 0; y < (sizeof(*lenet->weight2_3)/sizeof(*(*lenet->weight2_3))); ++y) 
		  for (int o0 = 0; o0 < (sizeof(features->layer3[y])/sizeof(*(features->layer3[y]))); ++o0)
		    for (int o1 = 0; o1 < (sizeof(*(features->layer3[y]))/sizeof(*(*(features->layer3[y])))); ++o1) 
				for (int w0 = 0; w0 < (sizeof(lenet->weight2_3[x][y])/sizeof(*(lenet->weight2_3[x][y]))); ++w0)
					for (int w1 = 0; w1 < (sizeof(*(lenet->weight2_3[x][y]))/sizeof(*(*(lenet->weight2_3[x][y])))); ++w1)
					 (features_fixed->layer3[y])[o0][o1] += (features_fixed->layer2[x])[o0 + w0][o1 + w1] * (lenet_fixed->weight2_3[x][y])[w0][w1]; 
	//activation / relu
	for (int j = 0; j < (sizeof(features->layer3)/sizeof(*(features->layer3))); ++j)
	   for (int i = 0; i < (sizeof(features->layer3[j])/sizeof(double)); ++i){
			//((double *)features->layer3[j])[i] = relu(((double *)features->layer3[j])[i] + lenet->bias2_3[j]); 
			((ap_temp_fixed_12 *)features_fixed->layer3[j])[i] = relu_fixed(((ap_temp_fixed_12 *)features_fixed->layer3[j])[i] + lenet_fixed->bias2_3[j]); 
			//~ ap_temp_fixed_6 relu_arg = ap_temp_fixed_6((((ap_temp_fixed_4 *)features_fixed->layer3[j])[i] + lenet_fixed->bias2_3[j]).getValueF()); 
			//~ ap_temp_fixed_6 relu_result = relu_fixed(relu_arg); 
			//~ ((ap_temp_fixed_4 *)features_fixed->layer3[j])[i] = ap_temp_fixed_4(relu_result.getValueF() );
		}
};
//subsampling2 
 { 	const int len0 = (sizeof(*(features->layer3))/sizeof(*(*(features->layer3)))) / (sizeof(*(features->layer4))/sizeof(*(*(features->layer4)))); 
	const int len1 = (sizeof(**(features->layer3))/sizeof(*(**(features->layer3)))) / (sizeof(**(features->layer4))/sizeof(*(**(features->layer4))));
	for (int i = 0; i < (sizeof(features->layer4)/sizeof(*(features->layer4))); ++i) 
		for (int o0 = 0; o0 < (sizeof(*(features->layer4))/sizeof(*(*(features->layer4)))); ++o0) 
			for (int o1 = 0; o1 < (sizeof(**(features->layer4))/sizeof(*(**(features->layer4)))); ++o1) {
				int x0 = 0, x1 = 0, ismax;
				for (int l0 = 0; l0 < len0; ++l0)
					for (int l1 = 0; l1 < len1; ++l1) { 
						ismax = features_fixed->layer3[i][o0*len0 + l0][o1*len1 + l1] > features_fixed->layer3[i][o0*len0 + x0][o1*len1 + x1];
						x0 += ismax * (l0 - x0);
						x1 += ismax * (l1 - x1); 
					} 
				features_fixed->layer4[i][o0][o1] = features_fixed->layer3[i][o0*len0 + x0][o1*len1 + x1]; 
			} 
 };
 //conv3
 { 	for (int x = 0; x < (sizeof(lenet->weight4_5)/sizeof(*(lenet->weight4_5))); ++x)
		for (int y = 0; y < (sizeof(*lenet->weight4_5)/sizeof(*(*lenet->weight4_5))); ++y)  
			for (int o0 = 0; o0 < (sizeof(features->layer5[y])/sizeof(*(features->layer5[y]))); ++o0) 
				for (int o1 = 0; o1 < (sizeof(*(features->layer5[y]))/sizeof(*(*(features->layer5[y])))); ++o1) 
					for (int w0 = 0; w0 < (sizeof(lenet->weight4_5[x][y])/sizeof(*(lenet->weight4_5[x][y]))); ++w0)
						for (int w1 = 0; w1 < (sizeof(*(lenet->weight4_5[x][y]))/sizeof(*(*(lenet->weight4_5[x][y])))); ++w1)
							(features_fixed->layer5[y])[o0][o1] += (features_fixed->layer4[x])[o0 + w0][o1 + w1] * (lenet_fixed->weight4_5[x][y])[w0][w1];  
//activation relu
	for (int j = 0; j < (sizeof(features->layer5)/sizeof(*(features->layer5))); ++j)
		for (int i = 0; i < (sizeof(features->layer5[j])/sizeof(double)); ++i){
			//((double *)features->layer5[j])[i] = relu(((double *)features->layer5[j])[i] + lenet->bias4_5[j]);
			((ap_temp_fixed_14 *)features_fixed->layer5[j])[i] = relu_fixed(((ap_temp_fixed_14 *)features_fixed->layer5[j])[i] + lenet_fixed->bias4_5[j]);
			//~ ap_temp_fixed_6 relu_arg = ap_temp_fixed_6((((ap_temp_fixed_4 *)features_fixed->layer5[j])[i] + lenet_fixed->bias4_5[j]).getValueF());
			//~ ap_temp_fixed_6 relu_result = relu_fixed(relu_arg);
			//~ ((ap_temp_fixed_4 *)features_fixed->layer5[j])[i] = relu_result.getValueF(); 
		}
 };
 //dot product
 {
	 for (int x = 0; x < (sizeof(lenet->weight5_6)/sizeof(*(lenet->weight5_6))); ++x) 
		for (int y = 0; y < (sizeof(*lenet->weight5_6)/sizeof(*(*lenet->weight5_6))); ++y) {
			//~ ((double *)features->output)[y] += ((double *)features->layer5)[x] * lenet->weight5_6[x][y];
			((ap_temp_fixed_15 *)features_fixed->output)[y] += ((ap_temp_fixed_14 *)features_fixed->layer5)[x] * lenet_fixed->weight5_6[x][y];
		}
	for (int j = 0; j < (sizeof(lenet->bias5_6)/sizeof(*(lenet->bias5_6))); ++j) {
		//~ ((double *)features->output)[j] = relu(((double *)features->output)[j] + lenet->bias5_6[j]); 
		((ap_temp_fixed_15 *)features_fixed->output)[j] = relu_fixed(((ap_temp_fixed_15 *)features_fixed->output)[j] + lenet_fixed->bias5_6[j]); 
		//~ ap_temp_fixed_6 relu_arg = (((ap_temp_fixed_5 *)features_fixed->output)[j] + lenet_fixed->bias5_6[j]).getValueF();
		//~ ap_temp_fixed_6 relu_result =  relu_fixed(relu_arg); 
		//~ ((ap_temp_fixed_5 *)features_fixed->output)[j] = relu_result.getValueF();
	}
};



}



static void backward(LeNet5 *lenet, LeNet5 *deltas, Feature *errors, Feature *features, double(*actiongrad)(double))
{
	DOT_PRODUCT_BACKWARD(features->layer5, errors->layer5, errors->output, lenet->weight5_6, deltas->weight5_6, deltas->bias5_6, actiongrad);
	CONVOLUTION_BACKWARD(features->layer4, errors->layer4, errors->layer5, lenet->weight4_5, deltas->weight4_5, deltas->bias4_5, actiongrad);
	SUBSAMP_MAX_BACKWARD(features->layer3, errors->layer3, errors->layer4);
	CONVOLUTION_BACKWARD(features->layer2, errors->layer2, errors->layer3, lenet->weight2_3, deltas->weight2_3, deltas->bias2_3, actiongrad);
	SUBSAMP_MAX_BACKWARD(features->layer1, errors->layer1, errors->layer2);
	CONVOLUTION_BACKWARD(features->input, errors->input, errors->layer1, lenet->weight0_1, deltas->weight0_1, deltas->bias0_1, actiongrad);
}

static inline void load_input(Feature *features, image input)
{
	double (*layer0)[LENGTH_FEATURE0][LENGTH_FEATURE0] = features->input;
	const long sz = sizeof(image) / sizeof(**input);
	double mean = 0, std = 0;
	FOREACH(j, sizeof(image) / sizeof(*input))
		FOREACH(k, sizeof(*input) / sizeof(**input))
	{
		mean += input[j][k];
		std += input[j][k] * input[j][k];
	}
	mean /= sz;
	std = sqrt(std / sz - mean*mean);
	FOREACH(j, sizeof(image) / sizeof(*input))
		FOREACH(k, sizeof(*input) / sizeof(**input))
	{
		layer0[0][j + PADDING][k + PADDING] = (input[j][k] - mean) / std;
	}
}

static inline void softmax(double input[OUTPUT], double loss[OUTPUT], int label, int count)
{
	double inner = 0;
	for (int i = 0; i < count; ++i)
	{
		double res = 0;
		for (int j = 0; j < count; ++j)
		{
			res += exp(input[j] - input[i]);
		}
		loss[i] = 1. / res;
		inner -= loss[i] * loss[i];
	}
	inner += loss[label];
	for (int i = 0; i < count; ++i)
	{
		loss[i] *= (i == label) - loss[i] - inner;
	}
}

static void load_target(Feature *features, Feature *errors, int label)
{
	double *output = (double *)features->output;
	double *error = (double *)errors->output;
	softmax(output, error, label, GETCOUNT(features->output));
}

static uint8 get_result(Feature_fixed *features_fixed, uint8 count)
{
	ap_temp_fixed_1 *output = (ap_temp_fixed_1 *)features_fixed->output; 
	const int outlen = GETCOUNT(features_fixed->output);
	uint8 result = 0;
	ap_temp_fixed_1 maxvalue = *output;
	for (uint8 i = 1; i < count; ++i)
	{
		if (output[i] > maxvalue)
		{
			maxvalue = output[i];
			result = i;
		}
	}
	return result;
}

static uint8 get_result(Feature *features, uint8 count)
{
	double *output = (double *)features->output; 
	const int outlen = GETCOUNT(features->output);
	uint8 result = 0;
	double maxvalue = *output;
	for (uint8 i = 1; i < count; ++i)
	{
		if (output[i] > maxvalue)
		{
			maxvalue = output[i];
			result = i;
		}
	}
	return result;
}

static double f64rand()
{
	static int randbit = 0;
	if (!randbit)
	{
		srand((unsigned)time(0));
		for (int i = RAND_MAX; i; i >>= 1, ++randbit);
	}
	unsigned long long lvalue = 0x4000000000000000L;
	int i = 52 - randbit;
	for (; i > 0; i -= randbit)
		lvalue |= (unsigned long long)rand() << i;
	lvalue |= (unsigned long long)rand() >> -i;
	return *(double *)&lvalue - 3;
}


void TrainBatch(LeNet5 *lenet, image *inputs, uint8 *labels, int batchSize)
{
	double buffer[GETCOUNT(LeNet5)] = { 0 };
	int i = 0;
#pragma omp parallel for
	for (i = 0; i < batchSize; ++i)
	{
		Feature features = { 0 };
		Feature errors = { 0 };
		LeNet5	deltas = { 0 };
		load_input(&features, inputs[i]);
		forward(lenet, &features, relu);
		load_target(&features, &errors, labels[i]);
		backward(lenet, &deltas, &errors, &features, relugrad);
		#pragma omp critical
		{
			FOREACH(j, GETCOUNT(LeNet5))
				buffer[j] += ((double *)&deltas)[j];
		}
	}
	double k = ALPHA / batchSize;
	FOREACH(i, GETCOUNT(LeNet5))
		((double *)lenet)[i] += k * buffer[i];
}

void Train(LeNet5 *lenet, image input, uint8 label)
{
	Feature features = { 0 };
	Feature errors = { 0 };
	LeNet5 deltas = { 0 };
	load_input(&features, input);
	forward(lenet, &features, relu);
	load_target(&features, &errors, label);
	backward(lenet, &deltas, &errors, &features, relugrad);
	FOREACH(i, GETCOUNT(LeNet5))
		((double *)lenet)[i] += ALPHA * ((double *)&deltas)[i];
}

uint8 Predict(LeNet5 *lenet, LeNet5_fixed* lenet_fixed, image input,uint8 count)
{
	Feature features = { 0 };
	Feature_fixed features_fixed = {0 };
	load_input(&features, input);
	{
		
	//double input[INPUT][LENGTH_FEATURE0][LENGTH_FEATURE0];	
		for(int i=0;i<INPUT; i++)
		for(int j=0;j<LENGTH_FEATURE0;j++)
		for(int k=0; k<LENGTH_FEATURE0; k++)
			features_fixed.input[i][j][k] = features.input[i][j][k];
	
	//double layer1[LAYER1][LENGTH_FEATURE1][LENGTH_FEATURE1];
		for(int i=0;i<LAYER1; i++)
		for(int j=0;j<LENGTH_FEATURE1;j++)
		for(int k=0; k<LENGTH_FEATURE1; k++)
			features_fixed.layer1[i][j][k] = features.layer1[i][j][k];	
	//double layer2[LAYER2][LENGTH_FEATURE2][LENGTH_FEATURE2]; 
		for(int i=0;i<LAYER2; i++)
		for(int j=0;j<LENGTH_FEATURE2;j++)
		for(int k=0; k<LENGTH_FEATURE2; k++)
			features_fixed.layer2[i][j][k] = features.layer2[i][j][k];	
	//double layer3[LAYER3][LENGTH_FEATURE3][LENGTH_FEATURE3];
		for(int i=0;i<LAYER3; i++)
		for(int j=0;j<LENGTH_FEATURE3;j++)
		for(int k=0; k<LENGTH_FEATURE3; k++)
			features_fixed.layer3[i][j][k] = features.layer3[i][j][k];	
		for(int i=0;i<LAYER4; i++)
		for(int j=0;j<LENGTH_FEATURE4;j++)
		for(int k=0; k<LENGTH_FEATURE4; k++)
			features_fixed.layer4[i][j][k] = features.layer4[i][j][k];	
		for(int i=0;i<LAYER5; i++)
		for(int j=0;j<LENGTH_FEATURE5;j++)
		for(int k=0; k<LENGTH_FEATURE5; k++)
			features_fixed.layer5[i][j][k] = features.layer5[i][j][k];	
	//double output[OUTPUT]; //[10]
		for(int i=0;i<OUTPUT; i++)
			features_fixed.output[i] = features.output[i];
			
	}
	//~ forward(lenet, &features, relu);
	forward_testing(lenet, lenet_fixed, &features,&features_fixed);
	
/*	
	//double layer1[LAYER1][LENGTH_FEATURE1][LENGTH_FEATURE1];
		for(int i=0;i<LAYER1; i++)
		for(int j=0;j<LENGTH_FEATURE1;j++)
		for(int k=0; k<LENGTH_FEATURE1; k++)
			printf(" %d %d %d, %.2f \n ", i,j,k,features_fixed.layer1[i][j][k].getValueF());	
		printf("\n");
	*/		
	return get_result(&features_fixed, count);
}

void Initial(LeNet5 *lenet)
{
	for (double *pos = (double *)lenet->weight0_1; pos < (double *)lenet->bias0_1; *pos++ = f64rand());
	for (double *pos = (double *)lenet->weight0_1; pos < (double *)lenet->weight2_3; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (INPUT + LAYER1))));
	for (double *pos = (double *)lenet->weight2_3; pos < (double *)lenet->weight4_5; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (LAYER2 + LAYER3))));
	for (double *pos = (double *)lenet->weight4_5; pos < (double *)lenet->weight5_6; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (LAYER4 + LAYER5))));
	for (double *pos = (double *)lenet->weight5_6; pos < (double *)lenet->bias0_1; *pos++ *= sqrt(6.0 / (LAYER5 + OUTPUT)));
	for (int *pos = (int *)lenet->bias0_1; pos < (int *)(lenet + 1); *pos++ = 0);
}
