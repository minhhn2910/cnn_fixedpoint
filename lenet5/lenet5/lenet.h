﻿/*
@author : 范文捷
@data    : 2016-04-20
@note	: 根据Yann Lecun的论文《Gradient-based Learning Applied To Document Recognition》编写
@api	:

批量训练
void TrainBatch(LeNet5 *lenet, image *inputs, const char(*resMat)[OUTPUT],uint8 *labels, int batchSize);

训练
void Train(LeNet5 *lenet, image input, const char(*resMat)[OUTPUT],uint8 label);

预测
uint8 Predict(LeNet5 *lenet, image input, const char(*resMat)[OUTPUT], uint8 count);

初始化
void Initial(LeNet5 *lenet);
*/

#pragma once

#define LENGTH_KERNEL	5

#define LENGTH_FEATURE0	32
#define LENGTH_FEATURE1	(LENGTH_FEATURE0 - LENGTH_KERNEL + 1) //28
#define LENGTH_FEATURE2	(LENGTH_FEATURE1 >> 1) //14
#define LENGTH_FEATURE3	(LENGTH_FEATURE2 - LENGTH_KERNEL + 1) //10
#define	LENGTH_FEATURE4	(LENGTH_FEATURE3 >> 1)// 5 
#define LENGTH_FEATURE5	(LENGTH_FEATURE4 - LENGTH_KERNEL + 1) //1

#define INPUT			1
#define LAYER1			6
#define LAYER2			6
#define LAYER3			16
#define LAYER4			16
#define LAYER5			120
#define OUTPUT          10

#define ALPHA 0.5
#define PADDING 2

typedef unsigned char uint8;
typedef uint8 image[28][28];


typedef struct LeNet5
{
	double weight0_1[INPUT][LAYER1][LENGTH_KERNEL][LENGTH_KERNEL]; // [1][6][5][5] 150
	double weight2_3[LAYER2][LAYER3][LENGTH_KERNEL][LENGTH_KERNEL]; // [6][16][5][5]  2400
	double weight4_5[LAYER4][LAYER5][LENGTH_KERNEL][LENGTH_KERNEL]; //[16][120][5][5]  48000
	double weight5_6[LAYER5 * LENGTH_FEATURE5 * LENGTH_FEATURE5][OUTPUT];//[120][10]  1200
//total weight : 51750
//total different kernels : 6 + 6*16 + 16*120 + 1 = 2023 groups /
	double bias0_1[LAYER1]; //6
	double bias2_3[LAYER3]; //16
	double bias4_5[LAYER5]; //120
	double bias5_6[OUTPUT]; //10
//total bias : 152
}LeNet5;

typedef struct Feature
{
	double input[INPUT][LENGTH_FEATURE0][LENGTH_FEATURE0]; //[1][32][32]  1024
	double layer1[LAYER1][LENGTH_FEATURE1][LENGTH_FEATURE1]; //[6][28][28] 4704
	double layer2[LAYER2][LENGTH_FEATURE2][LENGTH_FEATURE2]; //[6] [14][14]  1176
	double layer3[LAYER3][LENGTH_FEATURE3][LENGTH_FEATURE3]; //[16][10][10]   1600
	double layer4[LAYER4][LENGTH_FEATURE4][LENGTH_FEATURE4]; //[16][5][5]    400
	double layer5[LAYER5][LENGTH_FEATURE5][LENGTH_FEATURE5]; // [120][1][1]  120
	double output[OUTPUT]; //[10]
	//total layers: 9034
}Feature;

// Total Floating point values: 51750 + 152 + 9034s = 60936s
// 40s per test => 40 *60936 = 2437440sec = 28 days

void TrainBatch(LeNet5 *lenet, image *inputs, uint8 *labels, int batchSize);

void Train(LeNet5 *lenet, image input, uint8 label);

uint8 Predict(LeNet5 *lenet, image input, uint8 count);

void Initial(LeNet5 *lenet);
