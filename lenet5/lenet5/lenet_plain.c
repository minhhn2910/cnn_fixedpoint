
double relu(double x)
{
 return x*(x > 0);
}

double relugrad(double y)
{
 return y > 0;
}

static void forward(LeNet5 *lenet, Feature *features, double(*action)(double))
{
	//conv1 
 { for (int x = 0; x < (sizeof(lenet->weight0_1)/sizeof(*(lenet->weight0_1))); ++x)
	 for (int y = 0; y < (sizeof(*lenet->weight0_1)/sizeof(*(*lenet->weight0_1))); ++y) { 
		for (int o0 = 0; o0 < (sizeof(features->layer1[y])/sizeof(*(features->layer1[y]))); ++o0) 
		 for (int o1 = 0; o1 < (sizeof(*(features->layer1[y]))/sizeof(*(*(features->layer1[y])))); ++o1) 
			for (int w0 = 0; w0 < (sizeof(lenet->weight0_1[x][y])/sizeof(*(lenet->weight0_1[x][y]))); ++w0) 
				for (int w1 = 0; w1 < (sizeof(*(lenet->weight0_1[x][y]))/sizeof(*(*(lenet->weight0_1[x][y])))); ++w1)
					features->layer1[y][o0][o1] += features->input[x][o0 + w0][o1 + w1] * lenet->weight0_1[x][y][w0][w1]; }
		; for (int j = 0; j < (sizeof(features->layer1)/sizeof(*(features->layer1))); ++j)
		 for (int i = 0; i < (sizeof(features->layer1[j])/sizeof(double)); ++i)
			((double *)features->layer1[j])[i] = relu(((double *)features->layer1[j])[i] + lenet->bias0_1[j]);
 };
//sub sampling
 { const int len0 = (sizeof(*(features->layer1))/sizeof(*(*(features->layer1)))) / (sizeof(*(features->layer2))/sizeof(*(*(features->layer2))));
	 const int len1 = (sizeof(**(features->layer1))/sizeof(*(**(features->layer1)))) / (sizeof(**(features->layer2))/sizeof(*(**(features->layer2))));
	 for (int i = 0; i < (sizeof(features->layer2)/sizeof(*(features->layer2))); ++i)
	  for (int o0 = 0; o0 < (sizeof(*(features->layer2))/sizeof(*(*(features->layer2)))); ++o0) 
		for (int o1 = 0; o1 < (sizeof(**(features->layer2))/sizeof(*(**(features->layer2)))); ++o1) {
			 int x0 = 0, x1 = 0, ismax;
			  for (int l0 = 0; l0 < len0; ++l0) for (int l1 = 0; l1 < len1; ++l1) { 
				  ismax = features->layer1[i][o0*len0 + l0][o1*len1 + l1] > features->layer1[i][o0*len0 + x0][o1*len1 + x1];
				   x0 += ismax * (l0 - x0); x1 += ismax * (l1 - x1); } 
				   features->layer2[i][o0][o1] = features->layer1[i][o0*len0 + x0][o1*len1 + x1]; } 
 };
 //conv2 
 { for (int x = 0; x < (sizeof(lenet->weight2_3)/sizeof(*(lenet->weight2_3))); ++x)
	  for (int y = 0; y < (sizeof(*lenet->weight2_3)/sizeof(*(*lenet->weight2_3))); ++y) {
		   for (int o0 = 0; o0 < (sizeof(features->layer3[y])/sizeof(*(features->layer3[y]))); ++o0)
		    for (int o1 = 0; o1 < (sizeof(*(features->layer3[y]))/sizeof(*(*(features->layer3[y])))); ++o1) 
				for (int w0 = 0; w0 < (sizeof(lenet->weight2_3[x][y])/sizeof(*(lenet->weight2_3[x][y]))); ++w0)
					for (int w1 = 0; w1 < (sizeof(*(lenet->weight2_3[x][y]))/sizeof(*(*(lenet->weight2_3[x][y])))); ++w1)
					 (features->layer3[y])[o0][o1] += (features->layer2[x])[o0 + w0][o1 + w1] * (lenet->weight2_3[x][y])[w0][w1]; };
					 //activation / relu
					  for (int j = 0; j < (sizeof(features->layer3)/sizeof(*(features->layer3))); ++j)
					   for (int i = 0; i < (sizeof(features->layer3[j])/sizeof(double)); ++i)
					    ((double *)features->layer3[j])[i] = relu(((double *)features->layer3[j])[i] + lenet->bias2_3[j]); 
};
//subsampling2 
 { const int len0 = (sizeof(*(features->layer3))/sizeof(*(*(features->layer3)))) / (sizeof(*(features->layer4))/sizeof(*(*(features->layer4)))); 
	 const int len1 = (sizeof(**(features->layer3))/sizeof(*(**(features->layer3)))) / (sizeof(**(features->layer4))/sizeof(*(**(features->layer4))));
	  for (int i = 0; i < (sizeof(features->layer4)/sizeof(*(features->layer4))); ++i) 
	  for (int o0 = 0; o0 < (sizeof(*(features->layer4))/sizeof(*(*(features->layer4)))); ++o0) 
	  for (int o1 = 0; o1 < (sizeof(**(features->layer4))/sizeof(*(**(features->layer4)))); ++o1) {
		   int x0 = 0, x1 = 0, ismax;
		    for (int l0 = 0; l0 < len0; ++l0)
		     for (int l1 = 0; l1 < len1; ++l1) { 
					ismax = features->layer3[i][o0*len0 + l0][o1*len1 + l1] > features->layer3[i][o0*len0 + x0][o1*len1 + x1];
					x0 += ismax * (l0 - x0);
					x1 += ismax * (l1 - x1); 
				} 
			features->layer4[i][o0][o1] = features->layer3[i][o0*len0 + x0][o1*len1 + x1]; } 
 };
 //conv3
 { for (int x = 0; x < (sizeof(lenet->weight4_5)/sizeof(*(lenet->weight4_5))); ++x)
	  for (int y = 0; y < (sizeof(*lenet->weight4_5)/sizeof(*(*lenet->weight4_5))); ++y) { 
		  for (int o0 = 0; o0 < (sizeof(features->layer5[y])/sizeof(*(features->layer5[y]))); ++o0) 
			for (int o1 = 0; o1 < (sizeof(*(features->layer5[y]))/sizeof(*(*(features->layer5[y])))); ++o1) 
			for (int w0 = 0; w0 < (sizeof(lenet->weight4_5[x][y])/sizeof(*(lenet->weight4_5[x][y]))); ++w0)
			for (int w1 = 0; w1 < (sizeof(*(lenet->weight4_5[x][y]))/sizeof(*(*(lenet->weight4_5[x][y])))); ++w1)
			(features->layer5[y])[o0][o1] += (features->layer4[x])[o0 + w0][o1 + w1] * (lenet->weight4_5[x][y])[w0][w1]; }; 
			//activation relu
			for (int j = 0; j < (sizeof(features->layer5)/sizeof(*(features->layer5))); ++j)
			 for (int i = 0; i < (sizeof(features->layer5[j])/sizeof(double)); ++i)
				((double *)features->layer5[j])[i] = action(((double *)features->layer5[j])[i] + lenet->bias4_5[j]);
 };
 //dot product
 { for (int x = 0; x < (sizeof(lenet->weight5_6)/sizeof(*(lenet->weight5_6))); ++x) 
	 for (int y = 0; y < (sizeof(*lenet->weight5_6)/sizeof(*(*lenet->weight5_6))); ++y) 
		((double *)features->output)[y] += ((double *)features->layer5)[x] * lenet->weight5_6[x][y];
	for (int j = 0; j < (sizeof(lenet->bias5_6)/sizeof(*(lenet->bias5_6))); ++j) 
		((double *)features->output)[j] = action(((double *)features->output)[j] + lenet->bias5_6[j]); 
};
}

static void backward(LeNet5 *lenet, LeNet5 *deltas, Feature *errors, Feature *features, double(*actiongrad)(double))
{
 { for (int x = 0; x < (sizeof(lenet->weight5_6)/sizeof(*(lenet->weight5_6))); ++x) for (int y = 0; y < (sizeof(*lenet->weight5_6)/sizeof(*(*lenet->weight5_6))); ++y) ((double *)errors->layer5)[x] += ((double *)errors->output)[y] * lenet->weight5_6[x][y]; for (int i = 0; i < (sizeof(errors->layer5)/sizeof(double)); ++i) ((double *)errors->layer5)[i] *= actiongrad(((double *)features->layer5)[i]); for (int j = 0; j < (sizeof(errors->output)/sizeof(*(errors->output))); ++j) deltas->bias5_6[j] += ((double *)errors->output)[j]; for (int x = 0; x < (sizeof(lenet->weight5_6)/sizeof(*(lenet->weight5_6))); ++x) for (int y = 0; y < (sizeof(*lenet->weight5_6)/sizeof(*(*lenet->weight5_6))); ++y) deltas->weight5_6[x][y] += ((double *)features->layer5)[x] * ((double *)errors->output)[y]; };
 { for (int x = 0; x < (sizeof(lenet->weight4_5)/sizeof(*(lenet->weight4_5))); ++x) for (int y = 0; y < (sizeof(*lenet->weight4_5)/sizeof(*(*lenet->weight4_5))); ++y) { for (int i0 = 0; i0 < (sizeof(errors->layer5[y])/sizeof(*(errors->layer5[y]))); ++i0) for (int i1 = 0; i1 < (sizeof(*(errors->layer5[y]))/sizeof(*(*(errors->layer5[y])))); ++i1) for (int w0 = 0; w0 < (sizeof(lenet->weight4_5[x][y])/sizeof(*(lenet->weight4_5[x][y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(lenet->weight4_5[x][y]))/sizeof(*(*(lenet->weight4_5[x][y])))); ++w1) (errors->layer4[x])[i0 + w0][i1 + w1] += (errors->layer5[y])[i0][i1] * (lenet->weight4_5[x][y])[w0][w1]; }; for (int i = 0; i < (sizeof(errors->layer4)/sizeof(double)); ++i) ((double *)errors->layer4)[i] *= actiongrad(((double *)features->layer4)[i]); for (int j = 0; j < (sizeof(errors->layer5)/sizeof(*(errors->layer5))); ++j) for (int i = 0; i < (sizeof(errors->layer5[j])/sizeof(double)); ++i) deltas->bias4_5[j] += ((double *)errors->layer5[j])[i]; for (int x = 0; x < (sizeof(lenet->weight4_5)/sizeof(*(lenet->weight4_5))); ++x) for (int y = 0; y < (sizeof(*lenet->weight4_5)/sizeof(*(*lenet->weight4_5))); ++y) { for (int o0 = 0; o0 < (sizeof(deltas->weight4_5[x][y])/sizeof(*(deltas->weight4_5[x][y]))); ++o0) for (int o1 = 0; o1 < (sizeof(*(deltas->weight4_5[x][y]))/sizeof(*(*(deltas->weight4_5[x][y])))); ++o1) for (int w0 = 0; w0 < (sizeof(errors->layer5[y])/sizeof(*(errors->layer5[y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(errors->layer5[y]))/sizeof(*(*(errors->layer5[y])))); ++w1) (deltas->weight4_5[x][y])[o0][o1] += (features->layer4[x])[o0 + w0][o1 + w1] * (errors->layer5[y])[w0][w1]; }; };
 { const int len0 = (sizeof(*(errors->layer3))/sizeof(*(*(errors->layer3)))) / (sizeof(*(errors->layer4))/sizeof(*(*(errors->layer4)))); const int len1 = (sizeof(**(errors->layer3))/sizeof(*(**(errors->layer3)))) / (sizeof(**(errors->layer4))/sizeof(*(**(errors->layer4)))); for (int i = 0; i < (sizeof(errors->layer4)/sizeof(*(errors->layer4))); ++i) for (int o0 = 0; o0 < (sizeof(*(errors->layer4))/sizeof(*(*(errors->layer4)))); ++o0) for (int o1 = 0; o1 < (sizeof(**(errors->layer4))/sizeof(*(**(errors->layer4)))); ++o1) { int x0 = 0, x1 = 0, ismax; for (int l0 = 0; l0 < len0; ++l0) for (int l1 = 0; l1 < len1; ++l1) { ismax = features->layer3[i][o0*len0 + l0][o1*len1 + l1] > features->layer3[i][o0*len0 + x0][o1*len1 + x1]; x0 += ismax * (l0 - x0); x1 += ismax * (l1 - x1); } errors->layer3[i][o0*len0 + x0][o1*len1 + x1] = errors->layer4[i][o0][o1]; } };
 { for (int x = 0; x < (sizeof(lenet->weight2_3)/sizeof(*(lenet->weight2_3))); ++x) for (int y = 0; y < (sizeof(*lenet->weight2_3)/sizeof(*(*lenet->weight2_3))); ++y) { for (int i0 = 0; i0 < (sizeof(errors->layer3[y])/sizeof(*(errors->layer3[y]))); ++i0) for (int i1 = 0; i1 < (sizeof(*(errors->layer3[y]))/sizeof(*(*(errors->layer3[y])))); ++i1) for (int w0 = 0; w0 < (sizeof(lenet->weight2_3[x][y])/sizeof(*(lenet->weight2_3[x][y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(lenet->weight2_3[x][y]))/sizeof(*(*(lenet->weight2_3[x][y])))); ++w1) (errors->layer2[x])[i0 + w0][i1 + w1] += (errors->layer3[y])[i0][i1] * (lenet->weight2_3[x][y])[w0][w1]; }; for (int i = 0; i < (sizeof(errors->layer2)/sizeof(double)); ++i) ((double *)errors->layer2)[i] *= actiongrad(((double *)features->layer2)[i]); for (int j = 0; j < (sizeof(errors->layer3)/sizeof(*(errors->layer3))); ++j) for (int i = 0; i < (sizeof(errors->layer3[j])/sizeof(double)); ++i) deltas->bias2_3[j] += ((double *)errors->layer3[j])[i]; for (int x = 0; x < (sizeof(lenet->weight2_3)/sizeof(*(lenet->weight2_3))); ++x) for (int y = 0; y < (sizeof(*lenet->weight2_3)/sizeof(*(*lenet->weight2_3))); ++y) { for (int o0 = 0; o0 < (sizeof(deltas->weight2_3[x][y])/sizeof(*(deltas->weight2_3[x][y]))); ++o0) for (int o1 = 0; o1 < (sizeof(*(deltas->weight2_3[x][y]))/sizeof(*(*(deltas->weight2_3[x][y])))); ++o1) for (int w0 = 0; w0 < (sizeof(errors->layer3[y])/sizeof(*(errors->layer3[y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(errors->layer3[y]))/sizeof(*(*(errors->layer3[y])))); ++w1) (deltas->weight2_3[x][y])[o0][o1] += (features->layer2[x])[o0 + w0][o1 + w1] * (errors->layer3[y])[w0][w1]; }; };
 { const int len0 = (sizeof(*(errors->layer1))/sizeof(*(*(errors->layer1)))) / (sizeof(*(errors->layer2))/sizeof(*(*(errors->layer2)))); const int len1 = (sizeof(**(errors->layer1))/sizeof(*(**(errors->layer1)))) / (sizeof(**(errors->layer2))/sizeof(*(**(errors->layer2)))); for (int i = 0; i < (sizeof(errors->layer2)/sizeof(*(errors->layer2))); ++i) for (int o0 = 0; o0 < (sizeof(*(errors->layer2))/sizeof(*(*(errors->layer2)))); ++o0) for (int o1 = 0; o1 < (sizeof(**(errors->layer2))/sizeof(*(**(errors->layer2)))); ++o1) { int x0 = 0, x1 = 0, ismax; for (int l0 = 0; l0 < len0; ++l0) for (int l1 = 0; l1 < len1; ++l1) { ismax = features->layer1[i][o0*len0 + l0][o1*len1 + l1] > features->layer1[i][o0*len0 + x0][o1*len1 + x1]; x0 += ismax * (l0 - x0); x1 += ismax * (l1 - x1); } errors->layer1[i][o0*len0 + x0][o1*len1 + x1] = errors->layer2[i][o0][o1]; } };
 { for (int x = 0; x < (sizeof(lenet->weight0_1)/sizeof(*(lenet->weight0_1))); ++x) for (int y = 0; y < (sizeof(*lenet->weight0_1)/sizeof(*(*lenet->weight0_1))); ++y) { for (int i0 = 0; i0 < (sizeof(errors->layer1[y])/sizeof(*(errors->layer1[y]))); ++i0) for (int i1 = 0; i1 < (sizeof(*(errors->layer1[y]))/sizeof(*(*(errors->layer1[y])))); ++i1) for (int w0 = 0; w0 < (sizeof(lenet->weight0_1[x][y])/sizeof(*(lenet->weight0_1[x][y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(lenet->weight0_1[x][y]))/sizeof(*(*(lenet->weight0_1[x][y])))); ++w1) (errors->input[x])[i0 + w0][i1 + w1] += (errors->layer1[y])[i0][i1] * (lenet->weight0_1[x][y])[w0][w1]; }; for (int i = 0; i < (sizeof(errors->input)/sizeof(double)); ++i) ((double *)errors->input)[i] *= actiongrad(((double *)features->input)[i]); for (int j = 0; j < (sizeof(errors->layer1)/sizeof(*(errors->layer1))); ++j) for (int i = 0; i < (sizeof(errors->layer1[j])/sizeof(double)); ++i) deltas->bias0_1[j] += ((double *)errors->layer1[j])[i]; for (int x = 0; x < (sizeof(lenet->weight0_1)/sizeof(*(lenet->weight0_1))); ++x) for (int y = 0; y < (sizeof(*lenet->weight0_1)/sizeof(*(*lenet->weight0_1))); ++y) { for (int o0 = 0; o0 < (sizeof(deltas->weight0_1[x][y])/sizeof(*(deltas->weight0_1[x][y]))); ++o0) for (int o1 = 0; o1 < (sizeof(*(deltas->weight0_1[x][y]))/sizeof(*(*(deltas->weight0_1[x][y])))); ++o1) for (int w0 = 0; w0 < (sizeof(errors->layer1[y])/sizeof(*(errors->layer1[y]))); ++w0) for (int w1 = 0; w1 < (sizeof(*(errors->layer1[y]))/sizeof(*(*(errors->layer1[y])))); ++w1) (deltas->weight0_1[x][y])[o0][o1] += (features->input[x])[o0 + w0][o1 + w1] * (errors->layer1[y])[w0][w1]; }; };
}

static inline void load_input(Feature *features, image input)
{
 double (*layer0)[LENGTH_FEATURE0][LENGTH_FEATURE0] = features->input;
 const long sz = sizeof(image) / sizeof(**input);
 double mean = 0, std = 0;
 for (int j = 0; j < sizeof(image) / sizeof(*input); ++j)
  for (int k = 0; k < sizeof(*input) / sizeof(**input); ++k)
 {
  mean += input[j][k];
  std += input[j][k] * input[j][k];
 }
 mean /= sz;
 std = sqrt(std / sz - mean*mean);
 for (int j = 0; j < sizeof(image) / sizeof(*input); ++j)
  for (int k = 0; k < sizeof(*input) / sizeof(**input); ++k)
 {
  layer0[0][j + PADDING][k + PADDING] = (input[j][k] - mean) / std;
 }
}

static inline void softmax(double input[OUTPUT], double loss[OUTPUT], int label, int count)
{
 double inner = 0;
 for (int i = 0; i < count; ++i)
 {
  double res = 0;
  for (int j = 0; j < count; ++j)
  {
   res += exp(input[j] - input[i]);
  }
  loss[i] = 1. / res;
  inner -= loss[i] * loss[i];
 }
 inner += loss[label];
 for (int i = 0; i < count; ++i)
 {
  loss[i] *= (i == label) - loss[i] - inner;
 }
}

static void load_target(Feature *features, Feature *errors, int label)
{
 double *output = (double *)features->output;
 double *error = (double *)errors->output;
 softmax(output, error, label, (sizeof(features->output)/sizeof(double)));
}

static uint8 get_result(Feature *features, uint8 count)
{
 double *output = (double *)features->output;
 const int outlen = (sizeof(features->output)/sizeof(double));
 uint8 result = 0;
 double maxvalue = *output;
 for (uint8 i = 1; i < count; ++i)
 {
  if (output[i] > maxvalue)
  {
   maxvalue = output[i];
   result = i;
  }
 }
 return result;
}

static double f64rand()
{
 static int randbit = 0;
 if (!randbit)
 {
  srand((unsigned)time(0));
  for (int i = RAND_MAX; i; i >>= 1, ++randbit);
 }
 unsigned long long lvalue = 0x4000000000000000L;
 int i = 52 - randbit;
 for (; i > 0; i -= randbit)
  lvalue |= (unsigned long long)rand() << i;
 lvalue |= (unsigned long long)rand() >> -i;
 return *(double *)&lvalue - 3;
}


void TrainBatch(LeNet5 *lenet, image *inputs, uint8 *labels, int batchSize)
{
 double buffer[(sizeof(LeNet5)/sizeof(double))] = { 0 };
 int i = 0;
#pragma omp parallel for
 for (i = 0; i < batchSize; ++i)
 {
  Feature features = { 0 };
  Feature errors = { 0 };
  LeNet5 deltas = { 0 };
  load_input(&features, inputs[i]);
  forward(lenet, &features, relu);
  load_target(&features, &errors, labels[i]);
  backward(lenet, &deltas, &errors, &features, relugrad);
#pragma omp critical
  {
   for (int j = 0; j < (sizeof(LeNet5)/sizeof(double)); ++j)
    buffer[j] += ((double *)&deltas)[j];
  }
 }
 double k = ALPHA / batchSize;
 for (int i = 0; i < (sizeof(LeNet5)/sizeof(double)); ++i)
  ((double *)lenet)[i] += k * buffer[i];
}

void Train(LeNet5 *lenet, image input, uint8 label)
{
 Feature features = { 0 };
 Feature errors = { 0 };
 LeNet5 deltas = { 0 };
 load_input(&features, input);
 forward(lenet, &features, relu);
 load_target(&features, &errors, label);
 backward(lenet, &deltas, &errors, &features, relugrad);
 for (int i = 0; i < (sizeof(LeNet5)/sizeof(double)); ++i)
  ((double *)lenet)[i] += ALPHA * ((double *)&deltas)[i];
}

uint8 Predict(LeNet5 *lenet, image input,uint8 count)
{
 Feature features = { 0 };
 load_input(&features, input);
 forward(lenet, &features, relu);
 return get_result(&features, count);
}

void Initial(LeNet5 *lenet)
{
 for (double *pos = (double *)lenet->weight0_1; pos < (double *)lenet->bias0_1; *pos++ = f64rand());
 for (double *pos = (double *)lenet->weight0_1; pos < (double *)lenet->weight2_3; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (INPUT + LAYER1))));
 for (double *pos = (double *)lenet->weight2_3; pos < (double *)lenet->weight4_5; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (LAYER2 + LAYER3))));
 for (double *pos = (double *)lenet->weight4_5; pos < (double *)lenet->weight5_6; *pos++ *= sqrt(6.0 / (LENGTH_KERNEL * LENGTH_KERNEL * (LAYER4 + LAYER5))));
 for (double *pos = (double *)lenet->weight5_6; pos < (double *)lenet->bias0_1; *pos++ *= sqrt(6.0 / (LAYER5 + OUTPUT)));
 for (int *pos = (int *)lenet->bias0_1; pos < (int *)(lenet + 1); *pos++ = 0);
}
