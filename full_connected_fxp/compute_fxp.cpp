/*ap_temp_fixed_1 sigmoid_return;
ap_temp_fixed_2 sigmoid_n;
ap_temp_fixed_3 computeOutput_C_version_inputs;
ap_temp_fixed_4 computeOutput_C_version_output_Array;
ap_temp_fixed_5 computeOutput_C_version_weightsHolder0Aligned;
ap_temp_fixed_6 computeOutput_C_version_weightsHolder1Aligned;
ap_temp_fixed_7 computeOutput_C_version_weightsHolder2Aligned;
ap_temp_fixed_8 computeOutput_C_version_weightsHolder3Aligned;
ap_temp_fixed_9 computeOutput_C_version_weightsHolder4Aligned;
ap_temp_fixed_10 computeOutput_C_version_inputsAligned;
ap_temp_fixed_11 computeOutput_C_version_output0;
ap_temp_fixed_12 computeOutput_C_version_output1;
ap_temp_fixed_13 computeOutput_C_version_output2;
ap_temp_fixed_14 computeOutput_C_version_output3;
ap_temp_fixed_15 computeOutput_C_version_t_0;
ap_temp_fixed_16 computeOutput_C_version_u3_0;
ap_temp_fixed_17 computeOutput_C_version_t_1;
ap_temp_fixed_18 computeOutput_C_version_u3_1;
ap_temp_fixed_19 computeOutput_C_version_t_2;
ap_temp_fixed_20 computeOutput_C_version_u3_2;
ap_temp_fixed_21 computeOutput_C_version_t_3;
ap_temp_fixed_22 computeOutput_C_version_u3_3;
ap_temp_fixed_23 computeOutput_C_version_t_4;
ap_temp_fixed_24 computeOutput_C_version_u3_4;
*/

#include "compute_fxp.h"
#define LAYERIN 784
#define LAYER1 800
#define LAYER2 500

#define LAYER3 250
#define LAYER4 100
#define LAYEROUT 10

int init_readconfig() {
return 0;
}
ap_temp_fixed_1 inline sigmoid_fxp(ap_temp_fixed_2 n)
{
  //To deal with overflow rounding errors and the such
  if (n < -100)
      return 0;
  if (n > 100)
      return 1;
  //return 1/(1 + exp(-n));
  return 1/(1 + exp(0-float(n)));
}
float inline sigmoid(float n)
{
  ap_temp_fixed_2 n_temp = n;
  return float(sigmoid_fxp(n_temp));
}
void computeOutput_fxp_version(float *inputs,
					    ap_temp_fixed_4 *outputArray,
					    ap_temp_fixed_5 *weightsHolder0Aligned,
                                            ap_temp_fixed_6 *weightsHolder1Aligned,
                                            ap_temp_fixed_7 *weightsHolder2Aligned,
                                            ap_temp_fixed_8 *weightsHolder3Aligned,
                                            ap_temp_fixed_9 *weightsHolder4Aligned)
{
  //  float *inputsAligned = (float*)malloc(LAYERIN*sizeof(float));
  ap_temp_fixed_10 *inputsAligned = new ap_temp_fixed_10[LAYERIN];

    for(int i = 0; i < 784; i++)
        inputsAligned[i] = inputs[i];

  /*  float *output0 = (float*)malloc (LAYER1*sizeof(float));
    float *output1 = (float*)malloc (LAYER2*sizeof(float));
    float *output2 = (float*)malloc(LAYER3*sizeof(float));
    float *output3 =  (float*)malloc(LAYER4*sizeof(float));
  */
  ap_temp_fixed_11 *output0 = new ap_temp_fixed_11[LAYER1];
  ap_temp_fixed_12 *output1 = new ap_temp_fixed_12[LAYER2];
  ap_temp_fixed_13 *output2 = new ap_temp_fixed_13[LAYER3];
  ap_temp_fixed_14 *output3 = new ap_temp_fixed_14[LAYER4];


  //  computeOutput_Rolled_C_version(weightsHolder0Aligned, inputsAligned, output0, LAYER1, LAYERIN);
  {//avoid mistake of using predefined t, u;
      ap_temp_fixed_15 t_0;
      ap_temp_fixed_16 u3_0;

      t_0 = 0;

      for (unsigned int y = 0; y < LAYER1; y++)
      {
          u3_0 = 0;

          for (unsigned int k = 0; k < LAYERIN; k ++)
          {
              u3_0 += weightsHolder0Aligned[k + (y * LAYERIN)] * inputsAligned[k];
          }

          t_0 = sigmoid_fxp(u3_0);

          output0[y] = t_0;
      }
  }
  //    computeOutput_Rolled_C_version(weightsHolder1Aligned, output0, output1, LAYER2, LAYER1);

  {//avoid mistake of using predefined t, u;
        ap_temp_fixed_17 t_1;
        ap_temp_fixed_18 u3_1;

      t_1 = 0;

      for (unsigned int y = 0; y < LAYER2; y++)
      {
          u3_1= 0;

          for (unsigned int k = 0; k < LAYER1; k ++)
          {
              u3_1 += weightsHolder1Aligned[k + (y * LAYER1)] * output0[k];
          }

          t_1 = sigmoid_fxp(u3_1);

          output1[y] = t_1;
      }
  }

  //      computeOutput_Rolled_C_version(weightsHolder2Aligned, output1, output2, LAYER3, LAYER2);
  { //avoid mistake of using predefined t, u;
    ap_temp_fixed_19 t_2;
    ap_temp_fixed_20 u3_2;

  t_2 = 0;

  for (unsigned int y = 0; y < LAYER3; y++)
  {
      u3_2 = 0;

      for (unsigned int k = 0; k < LAYER2; k ++)
      {
          u3_2 += weightsHolder2Aligned[k + (y * LAYER2)] * output1[k];
      }

      t_2 = sigmoid_fxp(u3_2);

      output2[y] = t_2;
  }
  }
      //    computeOutput_Rolled_C_version(weightsHolder3Aligned, output2, output3, LAYER4, LAYER3);

      {//avoid reusing t, u
          ap_temp_fixed_21 t_3;
          ap_temp_fixed_22 u3_3;

          t_3 = 0;

          for (unsigned int y = 0; y < LAYER4; y++)
          {
              u3_3 = 0;

              for (unsigned int k = 0; k < LAYER3; k ++)
              {
                  u3_3 += weightsHolder3Aligned[k + (y * LAYER3)] * output2[k];
              }

              t_3 = sigmoid_fxp(u3_3);

              output3[y] = t_3;
          }
      }


  //          computeOutput_Rolled_C_version(weightsHolder4Aligned, output3, outputArray, LAYEROUT, LAYER4);
  {
      ap_temp_fixed_23 t_4;
      ap_temp_fixed_24 u3_4;

      t_4 = 0;

      for (unsigned int y = 0; y < LAYEROUT; y++)
      {
          u3_4 = 0;

          for (unsigned int k = 0; k < LAYER4; k ++)
          {
              u3_4 += weightsHolder4Aligned[k + (y * LAYER4)] * output3[k];
          }

          t_4 = sigmoid_fxp(u3_4);

          outputArray[y] = t_4;
      }
  }

  //printf("test");
    delete(output0);
    delete(output1);
    delete(output2);
    delete(output3);
    delete(inputsAligned);

}

void computeOutput_C_version(float *inputs,
					    float *outputArray,
					    float *weightsHolder0Aligned,
                                            float *weightsHolder1Aligned,
                                            float *weightsHolder2Aligned,
                                            float *weightsHolder3Aligned,
                                            float *weightsHolder4Aligned)
{

  //ap_temp_fixed_3 inputs_fxp = new ap_temp_fixed_3[];

  ap_temp_fixed_4 *outputArray_fxp = new ap_temp_fixed_4[LAYEROUT];


  ap_temp_fixed_5 *weightsHolder0Aligned_fxp = new ap_temp_fixed_5[LAYERIN*LAYER1];
  for (int i = 0; i < LAYERIN*LAYER1; i++){
    weightsHolder0Aligned_fxp[i] = weightsHolder0Aligned[i];
  }
  ap_temp_fixed_6 *weightsHolder1Aligned_fxp = new ap_temp_fixed_6[LAYER1*LAYER2];
  for (int i =0; i<LAYER1*LAYER2; i++){
    weightsHolder1Aligned_fxp[i] = weightsHolder1Aligned[i];
  }
  ap_temp_fixed_7 *weightsHolder2Aligned_fxp = new ap_temp_fixed_7[LAYER2*LAYER3];
  for (int i = 0; i<LAYER2*LAYER3; i++){
    weightsHolder2Aligned_fxp[i] = weightsHolder2Aligned[i];
  }
  ap_temp_fixed_8 *weightsHolder3Aligned_fxp = new ap_temp_fixed_8[LAYER3*LAYER4];
  for (int i =0 ;i<LAYER3*LAYER4; i++){
    weightsHolder3Aligned_fxp[i] = weightsHolder3Aligned[i];
  }
  ap_temp_fixed_9 *weightsHolder4Aligned_fxp = new ap_temp_fixed_9[LAYER4*LAYEROUT];
  for(int i =0; i< LAYER4*LAYEROUT; i++){
    weightsHolder4Aligned_fxp[i] = weightsHolder4Aligned[i];
  }
/*    float *weightsHolder0Aligned = new float[LAYERIN*LAYER1];
    float *weightsHolder1Aligned = new float[LAYER1*LAYER2];
    float *weightsHolder2Aligned = new float[LAYER2*LAYER3];
    float *weightsHolder3Aligned = new float[LAYER3*LAYER4];
    float *weightsHolder4Aligned = new float[LAYER4*LAYEROUT];*/

 computeOutput_fxp_version( inputs,
					    outputArray_fxp,
					    weightsHolder0Aligned_fxp,
                                            weightsHolder1Aligned_fxp,
                                            weightsHolder2Aligned_fxp,
                                            weightsHolder3Aligned_fxp,
                                            weightsHolder4Aligned_fxp);

  for (unsigned int y = 0; y < LAYEROUT; y++)
  {
      outputArray[y] = float(outputArray_fxp[y]);
  }
  delete (outputArray_fxp);
    delete (weightsHolder0Aligned_fxp);
 delete (weightsHolder1Aligned_fxp);
 delete (weightsHolder2Aligned_fxp);
 delete (weightsHolder3Aligned_fxp);
 delete (weightsHolder4Aligned_fxp);

}
