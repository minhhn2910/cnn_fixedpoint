#ifndef COMPUTE_FXP_H
#define COMPUTE_FXP_H
#include "fxp_meta.h"
int init_readconfig(void);
ap_temp_fixed_1 inline sigmoid_fxp(ap_temp_fixed_2);
float inline sigmoid(float );
void computeOutput_fxp_version(float *,
					    ap_temp_fixed_4 *,
					    ap_temp_fixed_5 *,
                                            ap_temp_fixed_6 *,
                                            ap_temp_fixed_7 *,
                                            ap_temp_fixed_8 *,
                                            ap_temp_fixed_9 *);


void computeOutput_C_version(float *,
					    float *,
					    float *,
                                            float *,
                                            float *,
                                            float *,
                                            float *);
#endif
