#ifndef PARSE_MNIST_H
#define PARSE_MNIST_H
#include "include.h"

//Functions to read MNIST data
int reverseInt(int i);
void readMNIST(vector<float*> &inputs, vector<int*> &targets, int &inputSize);
void readMNISTTest(vector<float*> &inputs, vector<int*> &targets, int &inputSize);

//Print input in matrix form input
void printInput(float* inputs);

//Functions to train and test a fully connected NN 
void testMNISTFullyConnectedNN();

#endif
