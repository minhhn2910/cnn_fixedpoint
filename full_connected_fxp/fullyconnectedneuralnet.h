#ifndef FULLYCONNECTED_H
#define FULLYCONNECTED_H
#include "include.h"
#include "layer.h"

//Class describing a fully connected neural net
class FullyConnectedNeuralNet
{
public:
    vector<int> netSpec;
    vector<Layer> layers;
    int status;

    int writeFileCounter;

    //Loads a net from a file
    void loadFullyConnectedNeuralNetFromFile(std::string netFileName);

    //Returns how large the net is in terms of bytes
    int getSizeOfNet();

    //Computese the output of the neural net given an array of inputs
    void computeOutput(float *inputs,
		       float *outputArray,
		       float *weightsArr0,
                       float *weightsArr1,
                       float *weightsArr2,
                       float *weightsArr3,
                       float *weightsArr4);

    void computeOutput_Rolled(float *weights,
			float *inputs,
			float *outputs,
			int layer_now,
			int layer_prev);

    //Calculates error
    void calcError(
        vector<float*> &trainingData,
        vector<int*> &trainingLabel);

private:
    int lastLayerIndex;
    size_t sizeOfNet;
    size_t sizeOfInput;
    size_t sizeOfTarget;
    size_t sizeOfOutput;
};
#endif
