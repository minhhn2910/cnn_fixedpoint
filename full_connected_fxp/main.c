//Static sizes for layers
#define LAYERIN 784
#define LAYER1 800
#define LAYER2 500
#define LAYER3 250
#define LAYER4 100
#define LAYEROUT 10

#include "include.h"
#include "MNIST.h"
#include "fullyconnectedneuralnet.h"
#include "layer.h"

/*
*######################
* FUNCTION PROTOTYPES #
*######################
*/

int main()
{
    testMNISTFullyConnectedNN();

    cout << "Enter any input to exit" << endl;
    int someInput;
    cin >> someInput;

    return EXIT_SUCCESS;
}

int reverseInt (int i)
{
    unsigned char c1, c2, c3, c4;

    c1 = i & 255;
    c2 = (i >> 8) & 255;
    c3 = (i >> 16) & 255;
    c4 = (i >> 24) & 255;

    return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
}

void readMNIST(vector<float*> &inputs, vector<int*> &targets, int &inputSize)
{
    //Read the targets
    // Set data dir before use
    std::ifstream targetFile("data/train-labels.idx1-ubyte", std::ios::binary);

    if (targetFile.is_open())
    {
        int magic_number=0;
        int number_of_images=0;
        int n_rows=0;
        int n_cols=0;

        targetFile.read((char*)&magic_number,sizeof(magic_number));
        magic_number= reverseInt(magic_number);

        targetFile.read((char*)&number_of_images,sizeof(number_of_images));
        number_of_images= reverseInt(number_of_images);

        for(int i=0;i != number_of_images;++i)
        {
            unsigned char temp=0;
            targetFile.read((char*)&temp,sizeof(temp));
            int* target = new int[10];
            std::fill_n(target,10,0);
            target[(int)temp] = 1;
            targets.push_back(target);

            if (i%10000 == 0)
                cout << i << "/" << number_of_images << endl;
        }
    }
    // Set data dir before use
    std::ifstream trainingFeatureFile("data/train-images.idx3-ubyte", std::ios::binary);

    if (trainingFeatureFile.is_open())
    {
        int magic_number=0;
        int number_of_images=0;
        int n_rows=0;
        int n_cols=0;

        trainingFeatureFile.read((char*)&magic_number,sizeof(magic_number));
        magic_number= reverseInt(magic_number);

        trainingFeatureFile.read((char*)&number_of_images,sizeof(number_of_images));
        number_of_images= reverseInt(number_of_images);

        trainingFeatureFile.read((char*)&n_rows,sizeof(n_rows));
        n_rows= reverseInt(n_rows);

        trainingFeatureFile.read((char*)&n_cols,sizeof(n_cols));
        n_cols= reverseInt(n_cols);

        inputSize = n_rows * n_cols;
        for(int i=0;i != number_of_images; ++i)
        {
            float* inputData = new float[n_rows*n_cols];
            for(int r=0;r<n_rows;++r)
            {
                for(int c=0;c<n_cols;++c)
                {
                    unsigned char temp=0;
                    trainingFeatureFile.read((char*)&temp,sizeof(temp));
                    inputData[r*n_cols + c] = ((float)temp)/255.0;
                }
            }
            inputs.push_back(inputData);
            if (i%10000 == 0)
                cout << i << "/" << number_of_images << endl;
        }
    }
}

void readMNISTTest(vector<float*> &inputs, vector<int*> &targets, int &inputSize)
{
    //Read the targets
    // Set data dir before use
    std::ifstream targetFile("data/t10k-labels.idx1-ubyte", std::ios::binary);

    if (targetFile.is_open())
    {
        int magic_number=0;
        int number_of_images=0;
        int n_rows=0;
        int n_cols=0;

        targetFile.read((char*)&magic_number,sizeof(magic_number));
        magic_number= reverseInt(magic_number);

        targetFile.read((char*)&number_of_images,sizeof(number_of_images));
        number_of_images= reverseInt(number_of_images);


        for(int i=0;i<number_of_images;++i)
        {
            unsigned char temp=0;
            targetFile.read((char*)&temp,sizeof(temp));
            int* target = new int[10];
            std::fill_n(target,10,0);
            target[(int)temp] = 1;
            targets.push_back(target);

            if (i%10000 == 0)
                cout << i << "/" << number_of_images << endl;
        }
    }

	// Set data directory before use
    std::ifstream trainingFeatureFile("data/t10k-images.idx3-ubyte", std::ios::binary);

    if (trainingFeatureFile.is_open())
    {
        int magic_number=0;
        int number_of_images=0;
        int n_rows=0;
        int n_cols=0;

        trainingFeatureFile.read((char*)&magic_number,sizeof(magic_number));
        magic_number= reverseInt(magic_number);

        trainingFeatureFile.read((char*)&number_of_images,sizeof(number_of_images));
        number_of_images= reverseInt(number_of_images);

        trainingFeatureFile.read((char*)&n_rows,sizeof(n_rows));
        n_rows= reverseInt(n_rows);

        trainingFeatureFile.read((char*)&n_cols,sizeof(n_cols));
        n_cols= reverseInt(n_cols);

        inputSize = n_rows * n_cols;
        for(int i=0;i<number_of_images;++i)
        {
            float* inputData = new float[n_rows*n_cols];
            for(int r=0;r<n_rows;++r)
            {
                for(int c=0;c<n_cols;++c)
                {
                    unsigned char temp=0;
                    trainingFeatureFile.read((char*)&temp,sizeof(temp));
                    inputData[r*n_cols + c] = ((float)temp)/255.0;
                }
            }
            inputs.push_back(inputData);
//            if (i%10000 == 0)
  //              cout << i << "/" << number_of_images << endl;
        }
    }
}

//Prints out an image matrix form
void printInput(float* inputs)
{
    cout << "BELOW IS AN IMAGE" << endl;
    int c = 0;
    for (int i = 0; i != 28; ++i)
    {
        cout << "    " << endl;
        for (int j = 0; j != 28; ++j)
        {
            if (inputs[c] > 0)
                cout << 1;
            else
                cout << 0;
            ++c;
        }
        cout << endl;
    }
}

void testMNISTFullyConnectedNN()
{
    vector<float*> testInputs;
    vector<int*> testTargets;
    int inputSize;

    cout << "Reading MNIST data set for testing..." << endl;
    readMNISTTest(testInputs,testTargets,inputSize);
    cout << "Finished reading. Now test" << endl;

    // Tests a single trained weights data file

    std::ofstream netFile;
    std::ostringstream fileNameStream;
    fileNameStream << "data/NN-MEDIUM.net"; // SET NET DIRECTORY HERE

    cout << "TESTING NET " << fileNameStream.str() << endl;

    FullyConnectedNeuralNet *myNet = new FullyConnectedNeuralNet;
    (*myNet).loadFullyConnectedNeuralNetFromFile(fileNameStream.str());
    (*myNet).calcError(testInputs, testTargets);

    delete myNet;
}

//Loads a neural net from a file
void FullyConnectedNeuralNet::loadFullyConnectedNeuralNetFromFile(std::string netFileName)
{
    //first parse the data
    std::ifstream netFile (netFileName, std::ios::in | std::ios::binary);
    string line;
    vector<vector<double> > weightVector;

    unsigned int lineN = 0;
    while (std::getline(netFile,line))
    {
        std::stringstream lineStream;
        lineStream << line;
        if (lineN == 0)
        {
            int val;
            int i = 0;
            while (lineStream >> val)
                netSpec.push_back(val);
            lineN = 1;
        }
        else
        {
            double val;
            vector<double> weights;
            while (lineStream >> val)
                weights.push_back(val);
            weightVector.push_back(weights);
        }
    }
    //Create the input layer
    Layer* inputLayer = layer_newInputLayer(netSpec[0]);
    layers.push_back(*inputLayer);

    //Create the rest of the layers
    int w = 0;
    for (unsigned int i = 1; i != netSpec.size(); ++i)
    {
        Layer* layer = layer_new(netSpec[i],netSpec[i-1]);
        for (int j = 0; j != (*layer).numberOfNodes; ++j)
        {
            for (int k = 0; k != (*layer).nodes[j].numberOfWeights; ++k)
                (*layer).nodes[j].weights[k] = weightVector[w][k];
            ++w;
        }
        layers.push_back(*layer);
    }

    writeFileCounter = 0;
    cout << "load successful" << endl;
}

/*
void FullyConnectedNeuralNet::computeOutput_Rolled(float* weights,
						float* inputs,
						float* outputs,
						int layer_now, int layer_prev)
{
    float t, u3;

    t = 0;

    for (unsigned int y = 0; y < layer_now; y++)
    {
        u3 = 0;

        for (unsigned int k = 0; k < layer_prev; k ++)
        {
            u3 += weights[k + (y * layer_prev)] * inputs[k];
        }

        t = sigmoid(u3);

        outputs[y] = t;
    }
}
*/
void computeOutput_C_version(float *inputs,
					    float *outputArray,
					    float *weightsHolder0Aligned,
                                            float *weightsHolder1Aligned,
                                            float *weightsHolder2Aligned,
                                            float *weightsHolder3Aligned,
                                            float *weightsHolder4Aligned)
{
  float *inputsAligned = (float*)malloc(LAYERIN*sizeof(float));

  for(int i = 0; i < 784; i++)
      inputsAligned[i] = inputs[i];

  float *output0 = (float*)malloc (LAYER1*sizeof(float));
  float *output1 = (float*)malloc (LAYER2*sizeof(float));
  float *output2 = (float*)malloc(LAYER3*sizeof(float));
  float *output3 =  (float*)malloc(LAYER4*sizeof(float));

  computeOutput_Rolled(weightsHolder0Aligned, inputsAligned, output0, LAYER1, LAYERIN);
    computeOutput_Rolled(weightsHolder1Aligned, output0, output1, LAYER2, LAYER1);
      computeOutput_Rolled(weightsHolder2Aligned, output1, output2, LAYER3, LAYER2);
        computeOutput_Rolled(weightsHolder3Aligned, output2, output3, LAYER4, LAYER3);
          computeOutput_Rolled(weightsHolder4Aligned, output3, outputArray, LAYEROUT, LAYER4);

  free(output0);
  free(output1);
  free(output2);
  free(output3);

}

void FullyConnectedNeuralNet::computeOutput(float *inputs,
					    float *outputArray,
					    float *weightsHolder0Aligned,
                                            float *weightsHolder1Aligned,
                                            float *weightsHolder2Aligned,
                                            float *weightsHolder3Aligned,
                                            float *weightsHolder4Aligned)
{
  computeOutput_C_version(inputs,
  					              outputArray,
          					      weightsHolder0Aligned,
                          weightsHolder1Aligned,
                          weightsHolder2Aligned,
                          weightsHolder3Aligned,
                          weightsHolder4Aligned);
  //   float *inputsAligned = new float[LAYERIN];
  //
  //   for(int i = 0; i < 784; i++)
  //       inputsAligned[i] = inputs[i];
  //
  //   float *output0 = new float[LAYER1];
  //   float *output1 = new float[LAYER2];
  //   float *output2 = new float[LAYER3];
  //   float *output3 = new float[LAYER4];
  //
  //   computeOutput_Rolled(weightsHolder0Aligned, inputsAligned, output0, LAYER1, LAYERIN);
	//     computeOutput_Rolled(weightsHolder1Aligned, output0, output1, LAYER2, LAYER1);
	// 	    computeOutput_Rolled(weightsHolder2Aligned, output1, output2, LAYER3, LAYER2);
	// 		    computeOutput_Rolled(weightsHolder3Aligned, output2, output3, LAYER4, LAYER3);
	// 			    computeOutput_Rolled(weightsHolder4Aligned, output3, outputArray, LAYEROUT, LAYER4);
  //
	// delete inputsAligned;
	// delete output0;
	// delete output1;
	// delete output2;
	// delete output3;
}

void calcError_C_Version()
{

}

void FullyConnectedNeuralNet::calcError(
    vector<float*> &trainingData,
    vector<int*> &trainingLabel)
{
    if (trainingLabel.size() != trainingData.size())
    {
        throw std::invalid_argument("training label and training data must have the same size!");
    }
    cout << "Calculating error" << endl;

    float* featureVector;
    int* targetVector;

    float *weightsHolder0Aligned = new float[LAYERIN*LAYER1];
    float *weightsHolder1Aligned = new float[LAYER1*LAYER2];
    float *weightsHolder2Aligned = new float[LAYER2*LAYER3];
    float *weightsHolder3Aligned = new float[LAYER3*LAYER4];
    float *weightsHolder4Aligned = new float[LAYER4*LAYEROUT];

    for(unsigned int asd = 0; asd < 800; asd++){
        for(unsigned int n = 0; n < 784; n++){
            weightsHolder0Aligned[(asd*784)+n] = layers[1].nodes[asd].weights[n];
        }
    }

    for(unsigned int asd = 0; asd < 500; asd++){
        for(unsigned int n = 0; n < 800; n++){
            weightsHolder1Aligned[(asd*800)+n] = layers[2].nodes[asd].weights[n];
        }
    }

    for(unsigned int asd = 0; asd < 250; asd++){
        for(unsigned int n = 0; n < 500; n++){
            weightsHolder2Aligned[(asd*500)+n] = layers[3].nodes[asd].weights[n];
        }
    }

    for(unsigned int asd = 0; asd < 100; asd++){
        for(unsigned int n = 0; n < 250; n++){
            weightsHolder3Aligned[(asd*250)+n] = layers[4].nodes[asd].weights[n];
        }
    }

    for(unsigned int asd = 0; asd < 10; asd++){
        for(unsigned int n = 0; n < 100; n++){
            weightsHolder4Aligned[(asd*100)+n] = layers[5].nodes[asd].weights[n];
        }
    }

    float errors = 0;
    unsigned int dataSetSize = trainingLabel.size();

    time_t start_time, end_time;
    //const double start_time = getCurrentTimestamp();

    time(&start_time);

    for (int i = 0; i != dataSetSize; i++)
    {
        if (i%10000 == 0)
        {
            cout << "    Checking input " << i << ". # Errors: " << errors << endl;
        }
        featureVector = trainingData[i];
		targetVector = trainingLabel[i];
		float *outputArray = new float[netSpec[netSpec.size()-1]];

        //First compute output
        computeOutput(featureVector, outputArray, weightsHolder0Aligned, weightsHolder1Aligned,
			 weightsHolder2Aligned, weightsHolder3Aligned, weightsHolder4Aligned);

        for (int index = 0; index != 10; index++)
        {
            int target = targetVector[index];

            if (outputArray[index] > 0.5 && target == 0)
            {
                errors += 1;
                break;
            }
            else if (outputArray[index] < 0.5 && target == 1)
            {
                errors += 1;
                break;
            }
        }

	delete outputArray;
    }

    //const double end_time = getCurrentTimestamp();
    time(&end_time);
    const double total_time = end_time - start_time;

    printf("Completed in: %0.3f s\n", total_time);
    printf("Average time per iteration (incl host runtimes): %.6f ms\n", ((total_time*1000)/dataSetSize));

    cout << "NUMBER OF ERRORS: " << errors << " ERROR RATE: " << 100*(errors / (float)dataSetSize) << "%" << endl;

    delete weightsHolder0Aligned;
    delete weightsHolder1Aligned;
    delete weightsHolder2Aligned;
    delete weightsHolder3Aligned;
    delete weightsHolder4Aligned;
}

//Function to return a random float between the supplied lower bound and upper bound
float getRandomFloat(float lowerbound, float upperbound)
{
    float f = (float)rand() / RAND_MAX;
    f = lowerbound + f * (upperbound - lowerbound);
    return f;
}

//Creates the input layer that has no nodes feeding into it
Layer* layer_newInputLayer(int numberOfNodes)
{
    Layer* netLayer = new Layer();
    netLayer->numberOfNodes = numberOfNodes;
    for (int i = 0; i != numberOfNodes; ++i)
    {
        Node node;
        node.numberOfWeights = 0;
        node.output = 0;
        netLayer->nodes[i] = node;
    }
    return netLayer;
}

//Creates a layer with nodes feeding into it
Layer* layer_new(int numberOfNodes, int numberOfWeights)
{
    Layer* netLayer = new Layer();
    (*netLayer).numberOfNodes = numberOfNodes;
    for (int i = 0; i != numberOfNodes; ++i)
    {
        Node* node = new Node();
        node->numberOfWeights = numberOfWeights;
        node->output = 0;
        for (int j = 0; j != numberOfWeights; ++j)
            node->weights[j] = getRandomFloat(-0.1,0.1);

        netLayer->nodes[i] = *node;
    }
    return netLayer;
}

float inline sigmoid(float n)
{
    //To deal with overflow rounding errors and the such
    if (n < -100)
        return 0;
    if (n > 100)
        return 1;
    return 1/(1 + exp(-n));
}


computeOutput_Rolled_C_version(float* weights,
						float* inputs,
						float* outputs,
						int layer_now, int layer_prev)
{
    float t, u3;

    t = 0;

    for (unsigned int y = 0; y < layer_now; y++)
    {
        u3 = 0;

        for (unsigned int k = 0; k < layer_prev; k ++)
        {
            u3 += weights[k + (y * layer_prev)] * inputs[k];
        }

        t = sigmoid(u3);

        outputs[y] = t;
    }
}

//calculate layer output using input
void FullyConnectedNeuralNet::computeOutput_Rolled(float* weights,
						float* inputs,
						float* outputs,
						int layer_now, int layer_prev)
{
    float t, u3;

    t = 0;

    for (unsigned int y = 0; y < layer_now; y++)
    {
        u3 = 0;

        for (unsigned int k = 0; k < layer_prev; k ++)
        {
            u3 += weights[k + (y * layer_prev)] * inputs[k];
        }

        t = sigmoid(u3);

        outputs[y] = t;
    }
}
