# README #

### What is this repository for? ###
Implementation of various cnns in fixedpoint version, facilitate precision tuning

* 1st step : Lenet based on the original repo on github : https://github.com/fan-wenjie/LeNet-5
* 2nd step : integrate into tinydnn [https://github.com/tiny-dnn/tiny-dnn]
### How do I get set up? ###
* The original version is in folder: lenet5/lenet5. Simply run the compile.sh script to compile the code. it will generate file binary : test
the file test will read mnist data and model.dat. If model.dat does not exist it will train the network from beginning.
It is recommended to use the model.dat file in this repo because if you train it again you may get different accuracy levels, difficult to track/ reproduce result.
Output of the original version : accuracy : e.g. 97% => error = 1-0.97 = 0.03
* The fixedpoint version is in lenet5/lenet-fixedpoint: it uses another (a little bit buggy) library in include/fixedpointto simulate arbitrary
precision fixedpoint arithmetic. Files are similar to the original version except some helper scripts & 3 important files:
	- temp_fxp_meta.h # where we declare fixed point variables, no need to change this file if we just want to run & test
	- iw_config.txt # contains the integer bitwidth of all the variables declared in temp_fxp_meta.h
	- fr_config.txt # contains the fraction bitwidth of all the variables declared in temp_fxp_meta.h

* To change bitwidth of a specific variable, simply change the value in number in iw_config.txt or fr_config.txt
* To know which variable in temp_fxp_meta.h map to which one in the original lenet, see lenet.h file 

* To run the code: check iw_config, fr_config, if you dont want to change their content, run the `./run.sh` script. It will automatically change header files, compile, and run 

Output of the fixedpoint version: error: e.g. output = 0.03 => accuracy = 97% classification

* The provided iw_config.txt and fr_config.txt are tuned for 0.03 error (original error ~ 0.025). So we sacrify 0.5% accuracy for fixedpoint conversion


### Contribution guidelines ###


### Who do I talk to? ###

